// Theme by default loads a jQuery as dependency of the main script.
// Let's include it using ES6 modules import.
import $ from 'jquery'
import Swiper, { Navigation, Pagination } from 'swiper';
// configure Swiper to use modules
Swiper.use([Navigation, Pagination]);
import SwiperAutoplay, {Autoplay} from 'swiper';

SwiperAutoplay.use([Autoplay]);
'use strict';

var TxtType = function(el, toRotate, period) {
  this.toRotate = toRotate;
  this.el = el;
  this.loopNum = 0;
  this.period = parseInt(period, 10) || 2000;
  this.txt = '';
  this.tick();
  this.isDeleting = false;
};

TxtType.prototype.tick = function() {
  var i = this.loopNum % this.toRotate.length;
  var fullTxt = this.toRotate[i];

  if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
  } else {
    this.txt = fullTxt.substring(0, this.txt.length + 1);
  }

  this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

  var that = this;
  var delta = 200 - Math.random() * 100;

  if (this.isDeleting) { delta /= 2; }

  if (!this.isDeleting && this.txt === fullTxt) {
    delta = this.period;
    this.isDeleting = true;
  } else if (this.isDeleting && this.txt === '') {
    this.isDeleting = false;
    this.loopNum++;
    delta = 500;
  }

  setTimeout(function() {
    that.tick();
  }, delta);
};

window.onload = function() {
  var elements = document.getElementsByClassName('home-slider__content-container__typewritter-container__typewritter');
  for (var i=0; i<elements.length; i++) {
    var toRotate = elements[i].getAttribute('data-type');
    var period = elements[i].getAttribute('data-period');
    if (toRotate) {
      new TxtType(elements[i], JSON.parse(toRotate), period);
    }
  }

  var css = document.createElement("style");
  css.type = "text/css";
  css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
  document.body.appendChild(css);
};

(function ($) {
  $(document).ready(function () {

    const swiper = new Swiper('.swiper-slider', {
      autoplay: {
        delay: 6000,
      },
      slidesPerView: 1,
      spaceBetween: 0,
      direction: 'horizontal',
      pagination: {
        el: ".swiper-slider-swiper-pagination",
        clickable: true,
        type: 'bullets',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      speed: 1000,
    });

    const reviews = new Swiper('.swiper-reviews', {
      autoplay: {
        delay: 20000,
      },
      slidesPerView: 1,
      spaceBetween: 30,
      direction: 'horizontal',
      pagination: {
        el: ".swiper-pagination",
      },
      speed: 1000,
      draggable: true,
      loop: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        // when window width is >= 320px
        480: {
          slidesPerView: 1,
        },
        640: {
          slidesPerView: 2,
          loop: true,
        },
        992: {
          slidesPerView: 3,
          loop: true,
        },
      }
    });

    const footer = new Swiper('.swiper-footer', {
      autoplay: {
        delay: 2000,
      },
      slidesPerView: 3,
      spaceBetween: 0,
      direction: 'horizontal',
      speed: 1000,
      draggable: true,
      breakpoints: {
        480: {
          slidesPerView: 3,
        },
        640: {
          slidesPerView: 5,
        },
        992: {
          slidesPerView: 8,
        },
      }
    });

    showPopup();

    function showPopup() {

      $('.show-modal').on('click', function (event) {
        event.preventDefault();
        var href = jQuery(this).attr('href');
        $('body').addClass('modal-open');
        $('.modal' + href).addClass('active');
        let agendaId = $('#agenda_id').val();
        $('input[name="agenda-item"]').attr('value',agendaId);
      });

      $('.modal .close-modal').on('click', function (event) {
        event.preventDefault();
        $('.modal').removeClass('active');
        $('body').removeClass('modal-open');
      });
    }

    $('.main-header__trigger').click(function () {
      $(this).toggleClass('active');
      $('.main-header__mobile-menu').toggleClass('active');
    });

    $('.main-header__mobile-menu li.menu-item-has-children').click(function () {
      $(this).toggleClass('active-menu-item');
    });

    $(document).keydown(function (e) {
      if (e.keyCode == 27) {
        $('.modal').removeClass('active');
        $('body').removeClass('modal-open');
      }
    });

    $(document).mouseup(function(e){
      var container = $(".modal__content");
      if(!container.is(e.target) && container.has(e.target).length === 0){
        e.preventDefault();
        $('.modal').removeClass('active');
        $('body').removeClass('modal-open');
      }
    });

  });
})(jQuery);

