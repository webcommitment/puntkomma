<?php use function Tonik\Theme\App\template;

get_header();

if ( ! is_front_page() ) :
    template( 'partials/page/page-header' );
endif;
?>

<section class="content-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="content">
                    <?php if ( have_posts() ) : ?>
                        <?php while ( have_posts() ) : the_post() ?>
                            <?php
                            /**
                             * Functions hooked into `theme/single/content` action.
                             *
                             * @hooked Tonik\Theme\App\Structure\render_post_content - 10
                             */
                            do_action( 'theme/single/content' );
                            ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
