<?php
/**
 * Reviews Block Template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'reviews-block-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}
$className = 'reviews-block';
if ( ! empty( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}

$content          = get_field( 'reviews_block_content' );
$background_image = get_field( 'reviews_background_image' );
$posts            = get_field( 'reviews_posts' );
?>

<section id="<?php esc_html_e( $id ); ?>"
         class="<?php esc_html_e( $className ); ?> full-width">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="<?php esc_html_e( $className ); ?>__content">
                    <?php echo $content; ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <?php if ( ! ( empty( $posts ) ) ): ?>
                    <div class="<?php esc_html_e( $className ); ?>__slides">
                        <div class="swiper-reviews">
                            <div class="swiper-wrapper">
                                <?php foreach ( $posts as $post ) :
                                    $logo = get_field( 'logo', $post );
                                    $review_score = get_field( 'review_score', $post );
                                    $review_name = get_field( 'reviewer_name', $post );
                                    $review_company = get_field( 'reviewer_company_name', $post );
                                    $post_content = get_post( $post )->post_content;
                                    $parent_id = wp_get_post_parent_id( $post );
                                    ?>
                                    <div class="swiper-slide">
                                        <div class="<?php esc_html_e( $className ); ?>__slides__slide">
                                            <div class="<?php esc_html_e( $className ); ?>__slides__slide__image">
                                                <?php echo wp_get_attachment_image( get_post_thumbnail_id( $post ),
                                                    'large' ); ?>
                                                <div class="<?php esc_html_e( $className ); ?>__slides__slide__image__score">
                                                    <span><?php echo $review_score; ?></span><img src="/wp-content/themes/wc-theme/resources/assets/images/star.svg" alt="star-score"/>
                                                </div>
                                            </div>
                                            <div class="<?php esc_html_e( $className ); ?>__slides__slide__container">
                                                <div
                                                    class="<?php esc_html_e( $className ); ?>__slides__slide__container__title">
                                                    <h3>
                                                        <?php echo get_the_title( $post ); ?>
                                                    </h3>
                                                </div>
                                                <p>
                                                    <?php echo $post_content; ?>
                                                </p>
                                                <div class="<?php esc_html_e( $className ); ?>__slides__slide__container__score">
                                                    <?php for ($i = 1; $i <= $review_score; $i++ ): ?>
                                                        <div class="<?php esc_html_e( $className ); ?>__slides__slide__container__score__star">
                                                            <img id="star-<?php echo $i; ?>" src="/wp-content/themes/wc-theme/resources/assets/images/star.svg" alt="star-score"/>
                                                        </div>
                                                    <?php endfor; ?>
                                                </div>
                                                <div class="<?php esc_html_e( $className ); ?>__slides__slide__container__info">
                                                    <strong><?php echo $review_name; ?></strong>
                                                    <span><?php echo $review_company; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="swiper-nav">
                                <div class="swiper-button-prev"></div>
                                <div class="swiper-button-next"></div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
