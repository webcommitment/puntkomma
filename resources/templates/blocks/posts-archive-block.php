<?php
/**
 * Posts Archive Block Template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

use function Tonik\Theme\App\template;

$content        = get_field( 'archive_content' );
$featured_posts = get_field( 'archive_posts' );
?>
<section class="posts-archive posts-archive--center">
    <div class="container-fluid">
        <div class="row">
            <div class="col ex-col">
                <div class="posts-archive__content">
                    <?php echo $content; ?>
                </div>
            </div>
        </div>
        <?php if ( $featured_posts ): ?>
            <div class="row">
                <div class="col">
                    <section class="posts-archive__inner">
                        <?php foreach ( $featured_posts as $post ) :
                            setup_postdata( $post );
                            template( 'partials/card/content', ['post' => $post] );
                        endforeach;
                        wp_reset_postdata();
                        ?>
                    </section>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>
