<?php
/**
 * Contact Form Block Template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'contact-form-block' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}
$className = 'contact-form-block';
if ( ! empty( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}

$content         = get_field( 'content_contact_form' );
$form            = get_field( 'contact_form' );
?>

<section id="<?php esc_html_e( $id ); ?>"
         class="<?php esc_html_e( $className ); ?> full-width">
    <div class="container-fluid">
        <?php if ( ! empty( $content ) ) : ?>
            <div class="row">
                <div class="col">
                    <div class="<?php esc_html_e( $className ); ?>__content">
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col">
                <div class="<?php esc_html_e( $className ); ?>__form">
                    <?php echo do_shortcode( '[contact-form-7 id="' . $form . '"]' ) ?>
                </div>
            </div>
        </div>
    </div>
</section>
