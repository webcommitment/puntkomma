<?php
/**
 * Map Block Template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'map-block-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}
$className = 'map-block';
if ( ! empty( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}

$map = get_field( 'map_code' );
?>

<section id="<?php esc_html_e( $id ); ?>"
         class="<?php esc_html_e( $className ); ?> full-width">
    <div class="<?php esc_html_e( $className ); ?>__content">
        <?php echo $map; ?>
    </div>
</section>
