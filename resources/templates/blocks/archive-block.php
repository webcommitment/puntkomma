<?php
/**
 * Archive Block Template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'archive-block-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}
$className = 'archive-block';
if ( ! empty( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}

$content = get_field( 'archive_block_content' );
$posts   = get_field( 'archive_posts' );
$link    = get_field( 'archive_link' );
?>

<section id="<?php esc_html_e( $id ); ?>"
         class="<?php esc_html_e( $className ); ?> full-width">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="<?php esc_html_e( $className ); ?>__content">
                    <?php echo $content; ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="<?php esc_html_e( $className ); ?>__archive">
                    <?php foreach ( $posts as $post ): ?>
                        <a href="<?php echo get_permalink( $post ); ?>"
                           class="<?php esc_html_e( $className ); ?>__archive__item">
                            <div class="<?php esc_html_e( $className ); ?>__archive__item__icon">
                                <?php echo wp_get_attachment_image( get_field( 'page_icon', $post ), 'medium' ); ?>
                            </div>
                            <div class="<?php esc_html_e( $className ); ?>__archive__item__title">
                                <?php echo get_the_title( $post ); ?>
                            </div>
                            <div class="<?php esc_html_e( $className ); ?>__archive__item__content">
                                <?php echo get_the_excerpt( $post ); ?>
                            </div>
                            <div class="<?php esc_html_e( $className ); ?>__archive__item__link">
                                <?php _e( 'Lees meer', 'wc-theme' ); ?>
                            </div>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="<?php esc_html_e( $className ); ?>__cta">
                    <?php if ( $link ):
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                        <a class="content-image-block__ctas__link"
                           href="<?php echo esc_url( $link_url ); ?>"
                           target="<?php echo esc_attr( $link_target ); ?>">
                            <span><?php echo esc_html( $link_title ); ?> ›</span>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
