<?php
/**
 * Content Block Template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'text-content-block-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}
$className = 'text-content-block';
if ( ! empty( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}

$content   = get_field( 'text_content_block' );
$alignment = get_field( 'text_content_alignment' );

if ( $alignment == 'Left' ) {
    $alignment = '--left';
} elseif ($alignment == 'Right') {
    $alignment = '--right';
} else {
    $alignment = '--center';
}

?>

<section id="<?php esc_html_e( $id ); ?>"
         class="<?php esc_html_e( $className ); ?> <?php esc_html_e( $className . $alignment ); ?> full-width">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="<?php esc_html_e( $className ); ?>__content">
                    <div><?php echo $content; ?></div>
                    <?php
                    if ( have_rows( 'text_content_ctas' ) ) : ?>
                        <div class="<?php esc_html_e( $className ); ?>__ctas">
                            <?php while ( have_rows( 'text_content_ctas' ) ) :
                                the_row();
                                $link        = get_sub_field( 'text_content_cta' ); ?>
                                <?php if ( $link ):
                                $link_url = $link['url'];
                                $link_title  = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>
                                <a class="<?php esc_html_e( $className ); ?>__ctas__link"
                                   href="<?php echo esc_url( $link_url ); ?>"
                                   target="<?php echo esc_attr( $link_target ); ?>">
                                    <span><?php echo esc_html( $link_title ); ?> ›</span>
                                </a>
                            <?php endif; ?>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
