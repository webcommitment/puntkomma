<?php
/**
 * Content Image Block Template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'content-image-block-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}
$className = 'content-image-block';
if ( ! empty( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}

$image     = get_field( 'content_block_image' );
$content   = get_field( 'content_block_content' );
$alignment = get_field( 'content_block_alignment' );

if ( $alignment == 'Left' ) {
    $alignment = '--left';
} else {
    $alignment = '--right';
}

?>

<section id="<?php esc_html_e( $id ); ?>"
         class="<?php esc_html_e( $className ); ?>  <?php esc_html_e( $className . $alignment ); ?> full-width">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 align-middle">
                <div class="<?php esc_html_e( $className ); ?>__content">
                    <div><?php echo $content; ?></div>
                    <?php
                    if ( have_rows( 'content_block_ctas' ) ) : ?>
                        <div class="content-image-block__ctas">
                            <?php while ( have_rows( 'content_block_ctas' ) ) :
                                the_row();
                                $link        = get_sub_field( 'content_block_ctas_link' ); ?>
                                <?php if ( $link ):
                                $link_url = $link['url'];
                                $link_title  = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>
                                <a class="content-image-block__ctas__link"
                                   href="<?php echo esc_url( $link_url ); ?>"
                                   target="<?php echo esc_attr( $link_target ); ?>">
                                    <span><?php echo esc_html( $link_title ); ?> ›</span>
                                </a>
                            <?php endif; ?>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-sm-6 align-middle">
                <div class="<?php esc_html_e( $className ); ?>__image">
                    <?php echo wp_get_attachment_image( $image, 'large' ); ?>
                </div>
            </div>
        </div>
    </div>
    <span class="divider"></span>
</section>
