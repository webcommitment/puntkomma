<?php
/**
 * CTA Block Template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'cta-block-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}
$className = 'cta-block';
if ( ! empty( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}
$additional_text = get_field( 'additional_text' );
?>

<section id="<?php esc_html_e( $id ); ?>"
         class="<?php esc_html_e( $className ); ?> full-width">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <?php
                if ( have_rows( 'block_ctas' ) ) : ?>
                    <div class="<?php esc_html_e( $className ); ?>__ctas">
                        <?php while ( have_rows( 'block_ctas' ) ) :
                            the_row();
                            $block_cta_icon           = get_sub_field( 'block_cta_icon' );
                            $block_cta_content_first  = get_sub_field( 'block_cta_content_first' );
                            $block_cta_content_second = get_sub_field( 'block_cta_content_second' );
                            $block_cta_link           = get_sub_field( 'block_cta_link' );
                            ?>
                            <a class="<?php esc_html_e( $className ); ?>__ctas__item"
                                href="<?php echo $block_cta_link; ?>"
                            >
                                <div class="<?php esc_html_e( $className ); ?>__ctas__item__icon">
                                    <?php echo wp_get_attachment_image( $block_cta_icon, 'thumbnail' ); ?>
                                </div>
                                <div class="<?php esc_html_e( $className ); ?>__ctas__item__content">
                                    <strong>
                                        <?php echo $block_cta_content_first; ?>
                                    </strong>
                                    <p>
                                        <?php echo $block_cta_content_second; ?>
                                    </p>
                                </div>
                            </a>
                        <?php endwhile; ?>
                        <div class="<?php esc_html_e( $className ); ?>__ctas__additional-text">
                            <?php echo $additional_text; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
