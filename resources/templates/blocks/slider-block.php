<?php
/**
 * Slider Block Template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$id = 'slider-block-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}
$className = 'slider-block';
if ( ! empty( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}

$content         = get_field( 'slider_block_content' );
$posts           = get_field( 'slider_posts' );
$additional_text = get_field( 'additional_text' );
?>

<section id="<?php esc_html_e( $id ); ?>"
         class="<?php esc_html_e( $className ); ?> full-width">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="<?php esc_html_e( $className ); ?>__content">
                    <?php echo $content; ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <?php if ( ! ( empty( $posts ) ) ): ?>
                    <div class="<?php esc_html_e( $className ); ?>__slides">
                        <div class="<?php esc_html_e( $className ); ?>__additional-text">
                            <?php echo $additional_text; ?>
                        </div>
                        <div class="swiper swiper-slider">
                            <div class="swiper-wrapper">
                                <?php foreach ( $posts as $post ) :
                                    $logo = get_field( 'logo', $post );
                                    $additional_icon = get_field( 'additional_icon', $post );
                                    $parent_id = wp_get_post_parent_id($post);
                                    ?>
                                    <div class="swiper-slide">
                                        <div class="<?php esc_html_e( $className ); ?>__slides__slide">
                                            <div class="<?php esc_html_e( $className ); ?>__slides__slide__image">
                                                <?php echo wp_get_attachment_image( get_post_thumbnail_id( $post ),
                                                    'large' ); ?>
                                                <?php if (! empty($additional_icon)) : ?>
                                                    <div class="<?php esc_html_e( $className ); ?>__slides__slide__image__overlay-icon">
                                                        <?php echo wp_get_attachment_image(  $additional_icon ,
                                                            'medium' ); ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                                <div class="<?php esc_html_e( $className ); ?>__slides__slide__container">
                                                    <div class="<?php esc_html_e( $className ); ?>__slides__slide__container__title">
                                                        <?php if (! empty($logo)) : ?>
                                                            <div class="<?php esc_html_e( $className ); ?>__slides__slide__container__title__logo">
                                                                <?php echo wp_get_attachment_image(  $logo ,
                                                                    'medium' ); ?>
                                                            </div>
                                                        <?php endif; ?>
                                                        <h3>
                                                            <?php echo get_the_title( $post ); ?>
                                                        </h3>
                                                    </div>
                                                    <p>
                                                        <?php echo get_the_excerpt( $post ); ?>
                                                    </p>
                                                    <a class="primary-btn"
                                                       href="<?php echo get_permalink( $post ); ?>">
                                                        <?php _e('Bekijk case','wc-theme'); ?>
                                                    </a>
                                                    <a class="tertiary-btn"
                                                        href="<?php echo get_permalink( $parent_id ); ?>">
                                                        <?php _e('Bekijk alle cases','wc-theme'); ?>
                                                    </a>
                                                </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="swiper-slider-swiper-pagination"></div>
                            <div class="swiper-nav">
                                <div class="swiper-button-prev"></div>
                                <div class="swiper-button-next"></div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <span class="divider"></span>
</section>
