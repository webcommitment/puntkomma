<?php
use function Tonik\Theme\App\template;
$post_id = get_queried_object_id();
$content = get_the_content( null, false, $post_id );

if ( have_posts() ): ?>

<section class="posts-archive posts-archive--center">
    <div class="container-fluid">
        <div class="row">
            <div class="col ex-col">
                <div class="posts-archive__content">
                    <?php echo $content; ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <section class="posts-archive__inner">
                    <?php while ( have_posts() ) : the_post();
                        template( 'partials/card/content' );
                    endwhile;
                    ?>
                </section>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
