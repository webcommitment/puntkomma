<?php

use function Tonik\Theme\App\template;

$post_id = get_field( 'workshop_archive_page', 'option' );
$content = get_the_content( null, false, $post_id );

$args = array(
	'post_type'      => 'workshop',
	'posts_per_page' => - 1,
	'meta_key'       => 'workshop_date',
	'orderby'        => 'meta_value',
	'order'          => 'DESC',
);

$query = new WP_Query( $args );
?>
<section class="posts-archive posts-archive--workshops posts-archive--center">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="posts-archive__content">
					<?php echo $content; ?>
                </div>
            </div>
        </div>
		<?php if ( $query->have_posts() ): ?>
            <div class="row">
                <div class="col">
                    <section class="posts-archive__inner">
						<?php while ( $query->have_posts() ) : $query->the_post();
							template( 'partials/card/content-workshop' );
						endwhile;
						?>
                    </section>
                </div>
            </div>
		<?php endif;
		wp_reset_postdata(); ?>
    </div>
</section>

