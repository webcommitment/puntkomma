<?php
$phone = get_field( 'footer_contacts_phone', 'option' );
$mail  = get_field( 'footer_contacts_mail', 'option' );
?>
<footer class="footer">
    <?php
    if ( have_rows( 'footer_logos', 'option' ) ) : ?>
        <section class="footer__logos">
            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <div class="swiper-footer">
                            <div class="swiper-wrapper">
                                <?php while ( have_rows( 'footer_logos', 'option' ) ) :
                                    the_row();
                                    $link            = get_sub_field( 'footer_logos_link', 'option' );
                                    $image           = get_sub_field( 'footer_logos_image', 'option' );

                                    if ( $link ) :
                                        $link_url = $link['url'];
                                        $link_target = $link['target'] ? $link['target'] : '_self';
                                        ?>
                                        <div class="swiper-slide">
                                            <a class="footer__logos__item"
                                               href="<?php echo esc_url( $link_url ); ?>"
                                               target="<?php echo esc_attr( $link_target ); ?>">
                                                <?php echo wp_get_attachment_image( $image, 'medium' ); ?>
                                            </a>
                                        </div>
                                    <?php endif; ?>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="footer__main full-width">
            <div class="footer__main__upper-bar">

            </div>
            <div class="footer__main__middle-bar">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col">
                            <div class="footer__main__middle-bar__inner">
                                <div class="footer__main__middle-bar__inner--left">
                                    <?php wp_nav_menu( [
                                        'theme_location' => 'footer-left',
                                    ] ); ?>
                                </div>
                                <div class="footer__main__middle-bar__inner--center">
                                    <a href="/">
                                        <?php echo get_custom_logo(); ?>
                                    </a>
                                </div>
                                <div class="footer__main__middle-bar__inner--right">
                                    <?php wp_nav_menu( [
                                        'theme_location' => 'footer-right',
                                    ] ); ?>
                                </div>
                                <div class="footer__main__middle-bar__inner--direct">
                                    <h4><?php _e( 'Direct contact', 'wc-theme' ); ?></h4>
                                    <div class="contacts">
                                        <?php
                                        if ( $phone ) :
                                            $link_url = $phone['url'];
                                            $link_title = $phone['title'];
                                            $link_target = $phone['target'] ? $link['target'] : '_self';
                                            ?>
                                            <a href="<?php echo esc_url( $link_url ); ?>"
                                               target="<?php echo esc_attr( $link_target ); ?>">
                                                <?php echo esc_html( $link_title ); ?>
                                            </a>
                                        <?php endif; ?>
                                        <?php
                                        if ( $mail ) :
                                            $link_url = $mail['url'];
                                            $link_title = $mail['title'];
                                            $link_target = $mail['target'] ? $link['target'] : '_self';
                                            ?>
                                            <a href="<?php echo esc_url( $link_url ); ?>"
                                               target="<?php echo esc_attr( $link_target ); ?>">
                                                <?php echo esc_html( $link_title ); ?>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                    <?php
                                    if ( have_rows( 'footer_social_media', 'option' ) ) : ?>
                                        <span>
                                            <?php while ( have_rows( 'footer_social_media', 'option' ) ) :
                                                the_row();
                                                $link            = get_sub_field( 'social_media_link', 'option' );
                                                $image           = get_sub_field( 'social_media_icon', 'option' );

                                                if ( $link ) :
                                                    $link_url = $link['url'];
                                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                                    ?>
                                                    <a class="footer__logos__item"
                                                       href="<?php echo esc_url( $link_url ); ?>"
                                                       target="<?php echo esc_attr( $link_target ); ?>">
                                                        <?php echo wp_get_attachment_image( $image, 'thumbnail' ); ?>
                                                    </a>
                                                <?php endif; ?>
                                            <?php endwhile; ?>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer__main__bottom-bar">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col">
                            <?php
                            if ( have_rows( 'footer_usps', 'option' ) ) : ?>
                                <div class="footer__main__bottom-bar__usps">
                                    <?php while ( have_rows( 'footer_usps', 'option' ) ) :
                                        the_row();
                                        $text = get_sub_field( 'footer_usp_text', 'option' );
                                        $icon = get_sub_field( 'footer_usp_icon', 'option' );
                                        ?>
                                        <div class="footer__main__bottom-bar__usps__item">
                                            <?php echo wp_get_attachment_image( $icon, 'thumbnail' ); ?>
                                            <?php echo $text; ?>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            <?php endif; ?>
                            <div class="footer__main__bottom-bar__sub-menu">
                                <?php wp_nav_menu( [
                                    'theme_location' => 'footer-submenu',
                                ] ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>
</footer>
</main>

<?php wp_footer(); ?>
</body>
</html>
