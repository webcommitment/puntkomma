<?php
$phone = get_field( 'footer_contacts_phone', 'option' );
$mail  = get_field( 'footer_contacts_mail', 'option' );
$wa  = get_field( 'footer_contacts_whatsapp', 'option' );
?>
<header class="main-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-6 col-lg-3">
                <div class="main-header__logo">
                        <?php
                        echo get_custom_logo(); ?>
                </div>
            </div>
            <div class="d-none d-lg-block col-lg-6">
                <nav id="primary-menu" class="main-header__primary-menu">
                    <?php wp_nav_menu( [
                        'theme_location' => 'primary',
                    ] ); ?>
                </nav>
            </div>
            <div class="col-lg-3 cta-absolute">
                <div class="main-header__cta">
                    <a id="mail" href="<?php echo $mail['url']; ?>">
                        <span><img src="/wp-content/themes/wc-theme/resources/assets/images/mail.svg"/></span><?php _e('Mail mij', 'wc-theme'); ?>
                    </a>
                    <a id="call" href="<?php echo $phone['url']; ?>" target="_blank" rel="nofollow">
                        <span><img src="/wp-content/themes/wc-theme/resources/assets/images/call.svg"/></span><?php _e('Bel mij', 'wc-theme'); ?>
                    </a>
                    <a id="whatsapp" href="<?php echo $wa['url']; ?>">
                        <img src="/wp-content/themes/wc-theme/resources/assets/images/whatsapp.svg"/>
                    </a>
                </div>
            </div>
            <div class="col-6 d-lg-none d-flex justify-content-end align-items-center">
                <div class="main-header__trigger">
                    <div></div>
                </div>
            </div>
        </div>
    </div>
</header>
<nav id="primary-menu" class="main-header__mobile-menu">
    <?php wp_nav_menu( [
        'theme_location' => 'primary',
    ] ); ?>
</nav>
