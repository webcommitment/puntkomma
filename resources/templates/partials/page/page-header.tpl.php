<?php $subtitle = get_field('page_subtitle'); ?>
<section class="page-header full-width">
    <div class="page-header__container">
        <div class="page-header__content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <span><?php echo $subtitle; ?></span>
                        <h1><?php the_title(); ?></h1>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-header__breadcrumbs">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="page-header__breadcrumbs__inner">
                        <?php yoast_breadcrumb(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
