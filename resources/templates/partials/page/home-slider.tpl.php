<?php
$home_slider_content        = get_field( 'home_slider_content' );
$home_slider_content_second = get_field( 'home_slider_content_second' );
$link                       = get_field( 'home_slider_cta' );
$typewriter_teksten         = get_field( 'typewriter_teksten' );
$home_slider_image          = get_field( 'home_slider_image' );
?>
<section class="home-slider">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="home-slider__content-container">
                    <?php if ( ! empty( $home_slider_content ) ): ?>
                        <strong class="home-slider__content-container__first-line">
                            <?php echo $home_slider_content; ?>
                        </strong>
                    <?php endif; ?>
                    <?php if ( ! empty( $home_slider_content_second ) ): ?>
                        <h1 class="home-slider__content-container__second-line">
                            <?php echo $home_slider_content_second; ?>
                        </h1>
                    <?php endif; ?>
                    <div class="home-slider__content-container__typewritter-container">
                        <?php if ( ! empty( $typewriter_teksten ) ): ?>
                            <div class="home-slider__content-container__typewritter-container__typewritter"
                                 data-period="2000"
                                 data-type='[ <?php echo $typewriter_teksten; ?> ]'>
                                <span class="wrap"></span>
                            </div>
                        <?php endif; ?>

                        <?php if ( $link ):
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                            <a class="home-slider__content-container__typewritter-container__cta primary-btn"
                               href="<?php echo esc_url( $link_url ); ?>"
                               target="<?php echo esc_attr( $link_target ); ?>">
                                        <span
                                            class="contact-team-member__link"><?php echo esc_html( $link_title ); ?> ›</span>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
                <?php if ( ! empty( $home_slider_image ) ): ?>
                    <div class="home-slider__image">
                        <?php echo wp_get_attachment_image( $home_slider_image, 'large' ); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <?php
                if ( have_rows( 'home_slider_usps' ) ) : ?>
                    <div class="home-slider__usps">
                        <?php while ( have_rows( 'home_slider_usps' ) ) :
                            the_row();
                            $home_slider_usp_icon           = get_sub_field( 'home_slider_usp_icon' );
                            $home_slider_usp_content_first  = get_sub_field( 'home_slider_usp_content_first' );
                            $home_slider_usp_content_second = get_sub_field( 'home_slider_usp_content_second' ); ?>
                            <div class="home-slider__usps__item">
                                <div class="home-slider__usps__item__icon">
                                    <?php echo wp_get_attachment_image( $home_slider_usp_icon, 'thumbnail' ); ?>
                                </div>
                                <div class="home-slider__usps__item__content">
                                    <strong>
                                        <?php echo $home_slider_usp_content_first; ?>
                                    </strong>
                                    <p>
                                        <?php echo $home_slider_usp_content_second; ?>
                                    </p>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
