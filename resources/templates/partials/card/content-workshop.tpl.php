<?php
$className = 'card-content';

if (!empty($post)) {
    $id = $post->ID;
} else {
    $id = get_the_ID();
}

$page_icon = get_field('page_icon', $id);
$additional_icon = get_field( 'additional_icon', $id );

$workshop_date = get_field('workshop_date');
$workshop_date_nl = get_field('workshop_date');

$workshop_date = str_replace(
	['jan', 'feb', 'mrt', 'apr', 'mei', 'jun', 'jul', 'aug', 'sep', 'okt', 'nov', 'dec'],
	['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'],
	strtolower($workshop_date)
);
$current_date = new DateTime('now', new DateTimeZone('Europe/Amsterdam'));
$date_object = DateTime::createFromFormat('d M Y', $workshop_date, new DateTimeZone('Europe/Amsterdam'));

$day = $date_object->format('d');
$month = $date_object->format('M');
$year = $date_object->format('Y');

$expired = false;

if ($date_object < $current_date) {
	$expired = true;
} else {
	$expired = false;
}

$workshop_full = get_field('workshop_full');

?>
<article class="<?php esc_html_e( $className ); if ( $expired === true ) { echo ' card-content--expired'; } ?>">
    <a class="<?php esc_html_e( $className ); ?>__container" href="<?php echo get_permalink($id); ?>">
        <div class="<?php esc_html_e( $className ); ?>__container__image">
            <?php echo wp_get_attachment_image( get_post_thumbnail_id($id), 'large' ); ?>
            <?php if (! empty($additional_icon)) : ?>
                <div class="<?php esc_html_e( $className ); ?>__container__image__overlay-icon">
                    <?php echo wp_get_attachment_image(  $additional_icon ,
                        'medium' ); ?>
                </div>
            <?php endif; ?>
            <?php if ( $workshop_full === true && $expired === false ) : ?>
                <div class="<?php esc_html_e( $className ); ?>__container__image__full <?php if( $expired === true ) { echo 'card-content__container__image__full--expired'; } ?>">
                    Deze workshop is vol
                </div>
            <?php endif; ?>
	        <?php if ( $expired === true ) : ?>
                <div class="<?php esc_html_e( $className ); ?>__container__image__full <?php if( $expired === true ) { echo 'card-content__container__image__full--expired'; } ?>">
                    Al geweest
                </div>
	        <?php endif; ?>
            <div class="<?php esc_html_e( $className ); ?>__container__image__icon <?php if( $expired === true ) { echo 'card-content__container__image__icon--expired'; } ?>">
                <span><?php echo $day; ?></span>
                <strong><?php echo $month; ?></strong>
                <span><?php echo $year; ?></span>
            </div>
        </div>
        <h2><?php echo get_the_title($id); ?></h2>
    </a>
</article>
