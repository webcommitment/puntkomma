<?php
$className = 'card-content';

if (!empty($post)) {
    $id = $post->ID;
} else {
    $id = get_the_ID();
}

$page_icon = get_field('page_icon', $id);
$additional_icon = get_field( 'additional_icon', $id );
?>
<article class="<?php esc_html_e( $className ); ?>">
    <a class="<?php esc_html_e( $className ); ?>__container"
        href="<?php echo get_permalink($id); ?>"
    >
        <div class="<?php esc_html_e( $className ); ?>__container__image">
            <?php echo wp_get_attachment_image( get_post_thumbnail_id($id), 'large' ); ?>
            <?php if (! empty($additional_icon)) : ?>
                <div class="<?php esc_html_e( $className ); ?>__container__image__overlay-icon">
                    <?php echo wp_get_attachment_image(  $additional_icon ,
                        'medium' ); ?>
                </div>
            <?php endif; ?>

            <?php if(! empty($page_icon)): ?>
                <div class="<?php esc_html_e( $className ); ?>__container__image__icon <?php esc_html_e( $className ); ?>__container__image__icon--service">
                    <?php echo wp_get_attachment_image( $page_icon, 'thumbnail' ); ?>
                </div>
            <?php else: ?>
                <div class="<?php esc_html_e( $className ); ?>__container__image__icon">
                    <span><?php echo get_the_date('j'); ?></span>
                    <strong><?php echo get_the_date('M'); ?></strong>
                    <span><?php echo get_the_date('Y'); ?></span>
                </div>
            <?php endif; ?>
        </div>
        <h2><?php echo get_the_title($id); ?></h2>
    </a>
</article>
