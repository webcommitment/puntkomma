<article class="single <?php if ( is_singular('workshop') ) { echo 'single--workshop'; } ?>">
    <?php if ( ! is_front_page() ): ?>
        <div class="single__post-content">
            <div class="single__post-content__post-info">
                <?php if ( ! is_page() ): ?>
                    <div>
                        <time class="single__post-content__post-info__time">
	                        <?php if ( is_singular('workshop') ) : ?>
                                <small><?php echo get_field('workshop_date'); ?></small>
                            <?php else : ?>
                                <small><?php the_date(); ?></small>
                            <?php endif; ?>
                        </time>
                    </div>
                <?php endif; ?>
            </div>
            <div class="single__post-content__content <?php if ( is_singular('workshop') ){ echo 'single__post-content__content--workshop'; } ?>">
                <?php the_content(); ?>
            </div>
        </div>
    <?php else: ?>
        <p><?php the_content(); ?></p>
    <?php endif; ?>
</article>
