<?php

namespace Tonik\Theme\Front;

/*
|------------------------------------------------------------------
| Page Controller
|------------------------------------------------------------------
|
| Think about theme template files as some sort of controllers
| from MVC design pattern. They should link application
| logic with your theme view templates files.
|
*/

use function Tonik\Theme\App\template;

/**
 * Renders Front page.
 *
 * @see resources/templates/front.tpl.php
 */
get_header();
template('partials/header');
template('partials/page/home-slider');
template('single');
get_footer();
