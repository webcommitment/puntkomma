<?php

namespace Tonik\Theme\Home;

/*
|------------------------------------------------------------------
| Page Controller
|------------------------------------------------------------------
|
| Think about theme template files as some sort of controllers
| from MVC design pattern. They should link application
| logic with your theme view templates files.
|
*/

use function Tonik\Theme\App\template;

/**
 * Renders archive page.
 *
 * @see resources/templates/archive.tpl.php
 */
get_header();
template('partials/header');
template( 'partials/page/page-header' );
template('archive-workshop');
get_footer();
