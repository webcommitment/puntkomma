<?php

namespace Tonik\Theme\App\Structure;

/*
|-----------------------------------------------------------
| Theme Custom Post Types
|-----------------------------------------------------------
|
| This file is for registering your theme post types.
| Custom post types allow users to easily create
| and manage various types of content.
|
*/

use function Tonik\Theme\App\config;

/**
 * Registers `workshop` custom post type.
 *
 * @return void
 */
function register_workshop_post_type()
{
    register_post_type('workshop', [
        'description' => __('Workshops', config('textdomain')),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'workshops'),
        'show_in_rest' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-welcome-learn-more',
        'supports' => ['title', 'editor', 'excerpt', 'thumbnail'],
        'labels' => [
            'name' => _x('Workshops', 'post type general name', config('textdomain')),
            'singular_name' => _x('Workshop', 'post type singular name', config('textdomain')),
            'menu_name' => _x('Workshops', 'admin menu', config('textdomain')),
            'name_admin_bar' => _x('Workshops', 'add new on admin bar', config('textdomain')),
            'add_new' => _x('Nieuwe workshop', 'workshop', config('textdomain')),
            'add_new_item' => __('Nieuwe workshop', config('textdomain')),
            'new_item' => __('Nieuwe workshop', config('textdomain')),
            'edit_item' => __('Edit workshop', config('textdomain')),
            'view_item' => __('Bekijk workshop', config('textdomain')),
            'all_items' => __('Alle workshops', config('textdomain')),
            'search_items' => __('Zoek workshop', config('textdomain')),
            'parent_item_colon' => __('Parent workshops:', config('textdomain')),
            'not_found' => __('Geen workshop gevonden.', config('textdomain')),
            'not_found_in_trash' => __('Geen workshop gevonden in de prullenbak.', config('textdomain')),
        ],
    ]);
}
add_action('init', 'Tonik\Theme\App\Structure\register_workshop_post_type');
