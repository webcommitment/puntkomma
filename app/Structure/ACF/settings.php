<?php
namespace Tonik\Theme\App\Structure\ACF;

use function Tonik\Theme\App\config;

acf_add_options_page([
    'page_title' => __('Algemene instellingen', config('wc-theme')),
    'menu_title' => __('Algemene instellingen', config('wc-theme')),
    'menu_slug' => 'general-settings',
    'capability' => 'edit_posts',
    'icon_url' => 'dashicons-admin-site',
    'redirect' => false
]);