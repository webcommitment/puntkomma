<?php

namespace Tonik\Theme\App\Setup;

/*
|-----------------------------------------------------------
| Theme Actions
|-----------------------------------------------------------
|
| This file purpose is to include your custom
| actions hooks, which process a various
| logic at specific parts of WordPress.
|
*/

function acf_init_block_types() {
    if ( function_exists( 'acf_register_block_type' ) ) {
        acf_register_block_type( array(
            'name'            => 'content-image-block',
            'title'           => __( 'Content Block with square image' ),
            'description'     => __( 'Content Block with square image with multiple settings.' ),
            'render_template' => 'resources/templates/blocks/content-image-block.php',
            'category'        => 'theme',
            'icon'            => 'format-aside',
            'keywords'        => array( 'content', 'image' ),
        ) );

        acf_register_block_type( array(
            'name'            => 'text-content-block',
            'title'           => __( 'Text Block' ),
            'description'     => __( 'Text Block' ),
            'render_template' => 'resources/templates/blocks/text-content-block.php',
            'category'        => 'theme',
            'icon'            => 'format-aside',
            'keywords'        => array( 'content', 'image' ),
        ) );

        acf_register_block_type( array(
            'name'            => 'archive-block',
            'title'           => __( 'Archive block' ),
            'description'     => __( 'Archive block with multiple settings.' ),
            'render_template' => 'resources/templates/blocks/archive-block.php',
            'category'        => 'theme',
            'icon'            => 'format-aside',
            'keywords'        => array( 'content', 'archive', 'services' ),
        ) );

        acf_register_block_type( array(
            'name'            => 'posts-archive-block',
            'title'           => __( 'Posts archive block' ),
            'description'     => __( 'Posts archive block with multiple settings.' ),
            'render_template' => 'resources/templates/blocks/posts-archive-block.php',
            'category'        => 'theme',
            'icon'            => 'format-aside',
            'keywords'        => array( 'content', 'archive', 'posts' ),
        ) );

        acf_register_block_type( array(
            'name'            => 'map-block',
            'title'           => __( 'Map block' ),
            'description'     => __( 'Map block' ),
            'render_template' => 'resources/templates/blocks/map-block.php',
            'category'        => 'theme',
            'icon'            => 'format-aside',
            'keywords'        => array( 'map', 'block' ),
        ) );

        acf_register_block_type( array(
            'name'            => 'slider-block',
            'title'           => __( 'Slider block' ),
            'description'     => __( 'Slider block with multiple settings.' ),
            'render_template' => 'resources/templates/blocks/slider-block.php',
            'category'        => 'theme',
            'icon'            => 'slides',
            'keywords'        => array( 'content', 'slider' ),
        ) );

        acf_register_block_type( array(
            'name'            => 'reviews-block',
            'title'           => __( 'Reviews block' ),
            'description'     => __( 'Reviews block with multiple settings.' ),
            'render_template' => 'resources/templates/blocks/reviews-block.php',
            'category'        => 'theme',
            'icon'            => 'star-filled',
            'keywords'        => array( 'content', 'reviews', 'slider' ),
        ) );

        acf_register_block_type( array(
            'name'            => 'cta-block',
            'title'           => __( 'CTA block' ),
            'description'     => __( 'CTA block with multiple settings.' ),
            'render_template' => 'resources/templates/blocks/cta-block.php',
            'category'        => 'theme',
            'icon'            => 'button',
            'keywords'        => array( 'content', 'cta' ),
        ) );

        acf_register_block_type( array(
            'name'            => 'contact-form-block',
            'title'           => __( 'Contact form block' ),
            'description'     => __( 'Contact form block with multiple settings.' ),
            'render_template' => 'resources/templates/blocks/contact-form-block.php',
            'category'        => 'theme',
            'icon'            => 'aside',
            'keywords'        => array( 'contact', 'form' ),
        ) );
    }
}
add_action( 'acf/init', 'Tonik\Theme\App\Setup\acf_init_block_types' );

function my_acf_op_init() {

    // Check function exists.
    if( function_exists('acf_add_options_page') ) {

        // Register options page.
        $option_page = acf_add_options_page(array(
            'page_title' 	=> __('General Options'),
            'menu_title' 	=> __('General Options'),
        ));
    }
}
add_action('acf/init', 'Tonik\Theme\App\Setup\my_acf_op_init');
